## Create or destroy droplet on DigitalOcean

- this is my personal use ansible role for managing dititalocean ressources
- it is highly experimantal yet
- I am not certain how far I will take it

---
### execute like: 

**create droplet**
```
ansible-playbook do_droplet.yml \
    -e '{ "state":"create" , "name":"my_droplet" , "project":"first-project" }' \ 
    --ask-vault-pass \
    -vvv
```

**destroy droplet**
```
ansible-playbook do_droplet.yml \
    -e '{ "state":"destroy" , "name":"my_droplet" , "project":"first-project" }' \ 
    --ask-vault-pass \
    -vvv
```
---

### currently it can 

  - create a droplet (within a given project)
  - destroy a droplet (by its unique name)
  - gather inforation about existing droplets


---

### To-Do:

- make a role for the current functionlaly and put 'create', 'destory', 'info' into separate task-files
